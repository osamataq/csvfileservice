package main;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import csvfileservice.CSVFileService;
import csvfileservice.DefaultCSVFileService;
public class Main {
	public static void testUpdate() {
		System.out.println("record updating test: ");
		Map<Integer, List<String>> query = new HashMap<Integer, List<String>>();
		query.put(1, Arrays.asList("2", "1"));
		Map<Integer, String> data = new HashMap<Integer, String>();
		data.put(3, "F");
		DefaultCSVFileService.getInstance().update(".//test.csv", ";", query, data);
		System.out.println(DefaultCSVFileService.getInstance().read(".//test.csv", ";", true).toString());
	}
	public static void testSearch() {
		System.out.println("record searching test: ");
		Map<Integer, List<String>> query = new HashMap<Integer, List<String>>();
		query.put(0, Arrays.asList("Osama Taqiedin", "Mohammad Nasro"));
		System.out.println(DefaultCSVFileService.getInstance().search(".//test.csv", ";", true, query).toString());
		query = new HashMap<Integer, List<String>>();
		query.put(1, Arrays.asList("3", "4"));
		System.out.println(DefaultCSVFileService.getInstance().search(".//test.csv", ";", true, query).toString());
	}
	public static void testDelete() {
		System.out.println("record deleting test: ");
		Map<Integer, List<String>> query = new HashMap<Integer, List<String>>();
		query.put(1, Arrays.asList("3", "4"));
		query.put(2, Arrays.asList("Liverpool"));
		DefaultCSVFileService.getInstance().delete(".//test.csv",";", true, query);
		System.out.println(DefaultCSVFileService.getInstance().read(".//test.csv", ";", true).toString());
	}
	public static void testAdd() {
		List<String> row = new ArrayList<String>();
		row.add("Basher");
		row.add("5");
		row.add("Zarqa");
		row.add("C-");
		DefaultCSVFileService.getInstance().add(".//test.csv", ";", row);
		System.out.println("record adding test: ");
		System.out.println(DefaultCSVFileService.getInstance().read(".//test.csv", ";", true).toString());
	}
	public static void testWrite() {
		List<List<String>> fileContent = new ArrayList<List<String>>();
		fileContent.add(Arrays.asList("Username", "ID", "Address", "Grade"));
		fileContent.add(Arrays.asList("Osama Taqiedin", "1", "Amman", "A+"));
		fileContent.add(Arrays.asList("Mohammad Nasro", "2", "Amman", "A-"));
		fileContent.add(Arrays.asList("Mohammad Salah", "3", "Liverpool", "B+"));
		fileContent.add(Arrays.asList("Kazem Al-Saher", "4", "Cairo", "B-"));
		DefaultCSVFileService.getInstance().write(".//test.csv", ";", fileContent, true);
	}
	public static void testRead() {
		System.out.println("file reading test: ");
		System.out.println(DefaultCSVFileService.getInstance().read(".//test.csv", ";", true).toString());
	}
	public static void main(String[] args) {
		testWrite();
		testRead();
		testAdd();
		testSearch();
		testDelete();
		testUpdate();
		System.out.println("The file is located in the project root folder and is called test.csv");
	}
}
