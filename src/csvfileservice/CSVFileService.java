package csvfileservice;
import java.util.List;
import java.util.Map;

public interface CSVFileService {
	List<List<String>> read(String filePath, String delimiter, Boolean includeHeader);
	List<List<String>> search(String filePath, String delimiter, Boolean includeHeader, String... keyword);
	List<List<String>> search(String filePath, String delimiter, Boolean includeHeader, Map<Integer, List<String>> query);
	Boolean write(String filePath, String delimiter, List<List<String>> fileContent, Boolean overwrite);
	void update(String filePath, String delimiter, Map<Integer, List<String>> query, Map<Integer, String> UpdatedData);
	void add(String filePath, String delimiter, List<String> row);
	Boolean delete(String filePath, String delimiter, Boolean includeHeader, Map<Integer, List<String>> query);
};