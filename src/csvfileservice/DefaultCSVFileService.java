package csvfileservice;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DefaultCSVFileService implements CSVFileService {
	private static DefaultCSVFileService Service;
	
	private DefaultCSVFileService() {
	}
	
	public static DefaultCSVFileService getInstance() {
		if(Service == null)
			Service = new DefaultCSVFileService();
		return Service;
	}
	

	@Override
	public List<List<String>> read(String filePath, String delimiter, Boolean includeHeader) {
		List<List<String>> fileContent = new ArrayList<>();
		if(filePath == null || delimiter == null || includeHeader == null) {
			return fileContent;
		}
		try(BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
			List<String> lines = new ArrayList<>();
			String line = reader.readLine();
			if(includeHeader) {
				lines.add(line);
			}
			while(true) {
				line = reader.readLine();
				if(line == null)
					break;
				lines.add(line);
			}
			for(int i = 0;i<lines.size();i++) {
				fileContent.add(Arrays.asList(lines.get(i).split(delimiter)));
			}
			return fileContent;
		}
		catch(FileNotFoundException ex) {
			System.out.println("The file at: " + filePath + " does not exist, please check again!");
		} catch (IOException ex) {
			System.out.println("An unspecific error occured when reading the file at: " + filePath+ " ,please try again!");
		}
		return new ArrayList<>();
	}

	@Override
	public List<List<String>> search(String filePath, String delimiter, Boolean includeHeader, String... keywords) {
		List<List<String>> fileContent = DefaultCSVFileService.getInstance().read(filePath, delimiter, includeHeader);
		if(fileContent.size() == 0) {
			return fileContent;
		}
		List<List<String>> filteredFileContent = new ArrayList<List<String>>();
		Integer offset = 0;
		if(includeHeader) {
			filteredFileContent.add(fileContent.get(0));
			offset = 1;
		}
		for(int i = offset;i<fileContent.size();i++) {
			for(int j = 0;j<fileContent.get(i).size();j++) {
				if(Arrays.asList(keywords).contains(fileContent.get(i).get(j))) {
					filteredFileContent.add(fileContent.get(i));
					break;
				}
			}
		}
		return filteredFileContent;
	}

	@Override
	public List<List<String>> search(String filePath, String delimiter, Boolean includeHeader,
			Map<Integer, List<String>> query) {
		List<List<String>> fileContent = DefaultCSVFileService.getInstance().read(filePath, delimiter, includeHeader);
		if(fileContent.size() == 0) {
			return fileContent;
		}
		List<List<String>> filteredFileContent = new ArrayList<List<String>>();
		Integer offset = 0;
		if(includeHeader) {
			filteredFileContent.add(fileContent.get(0));
			offset = 1;
		}
		for(int i = offset;i<fileContent.size();i++) {
			for(int j = 0;j<fileContent.get(i).size();j++) {
				if(query.get(j) != null) {
					if(query.get(j).contains(fileContent.get(i).get(j))) {
						filteredFileContent.add(fileContent.get(i));
						break;
					}
				}
			}
		}
		return filteredFileContent;
	}

	@Override
	public Boolean write(String filePath, String delimiter, List<List<String>> fileContent, Boolean overWrite) {
		if(filePath == null || delimiter == null || fileContent.size() == 0) {
			return false;
		}
		try(BufferedWriter writer = new BufferedWriter(new FileWriter(filePath, !overWrite))){
			for(int i = 0;i<fileContent.size();i++) {
				StringBuilder builder = new StringBuilder();
				for(int j = 0;j<fileContent.get(i).size();j++) {
					if(j != 0)
						builder.append(delimiter);
					builder.append(fileContent.get(i).get(j));
				}
				builder.append("\n");
				writer.append(builder.toString());
			}
			return true;
		}
		catch(FileNotFoundException ex) {
			System.out.println("The file at: " + filePath + " does not exist, please check again!");
		} catch (IOException ex) {
			System.out.println("An unspecific error occured when writing the file at: " + filePath+ " ,please try again!");
		}
		return false;
	}

	@Override
	public void update(String filePath, String delimiter,  Map<Integer, List<String>> query, Map<Integer, String> UpdatedData) {
		if(filePath == null || delimiter == null)
			return;
		List<List<String>> queryResult = DefaultCSVFileService.getInstance().search(filePath, delimiter, false, query);
		DefaultCSVFileService.getInstance().delete(filePath, delimiter, false, query);
		for(int i = 0;i<queryResult.size();i++) {
			for(int j = 0;j<queryResult.get(i).size();j++) {
				if(UpdatedData.get(j) != null) {
					queryResult.get(i).set(j, UpdatedData.get(j));
				}
			}
		}
		DefaultCSVFileService.getInstance().write(filePath, delimiter, queryResult, false);
	}

	@Override
	public void add(String filePath, String delimiter, List<String> row) {
		if(filePath == null || delimiter == null || row.size() == 0)
			return;
		List<List<String>> tempArray = new ArrayList<List<String>>();
		tempArray.add(row);
		DefaultCSVFileService.getInstance().write(filePath, delimiter, tempArray , false);
	}

	@Override
	public Boolean delete(String filePath, String delimiter, Boolean includeHeader, Map<Integer, List<String>> query) {
		List<List<String>> fileContent = DefaultCSVFileService.getInstance().read(filePath, delimiter, true);
		if(fileContent.size() == 0) {
			return false;
		}
		List<List<String>> filteredFileContent = new ArrayList<List<String>>();
		filteredFileContent.add(fileContent.get(0));
		for(int i = 1;i<fileContent.size();i++) {
			Boolean flag = true;
			for(int j = 0;j<fileContent.get(i).size();j++) {
				if(query.get(j) != null) {
					if(query.get(j).contains(fileContent.get(i).get(j))) {
						flag = false;
						break;
					}
				}
			}
			if(flag) {
				filteredFileContent.add(fileContent.get(i));
			}
		}
		return DefaultCSVFileService.getInstance().write(filePath, delimiter, filteredFileContent, true);
	}

}
